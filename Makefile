# because autotools is scary...

PCS=gtk+-2.0
OBJS=main.o
TGT=nebulous

CFLAGS+=-Wall $(shell pkg-config --cflags $(PCS))
LDFLAGS+=$(shell pkg-config --libs $(PCS))

$(TGT): $(OBJS)
	$(CC) $(LDFLAGS) -o $@ $(OBJS)

main.o:

.PHONY: clean
clean:
	rm -f $(TGT) $(OBJS)
