/*
 * Copyright © 2008 Christopher Eby <kreed@kreed.org>
 *
 * This file is part of Nebulous.
 *
 * Nebulous is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Nebulous is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See <http://www.gnu.org/licenses/> for the full license text.
 */

#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
	const char *hint;
	const char *truth;
	char **truths;
} card;

static GPtrArray *cards;
static GtkWidget *count;
static GtkWidget *label;
static GtkWidget *entry;

typedef enum {
	WAITING = 0,
	ADVANCE_AND_DELETE,
	ADVANCE_AND_KEEP
} state;

static state status;

static card *current;

static void
add_element(const char *e, const char *v)
{
	card *new = g_new(card, 1);
	new->hint = g_strdup(e);
	new->truth = g_strdup(v);
	new->truths = g_strsplit_set(v, " ;,", 0);
	g_ptr_array_add(cards, new);
}

static void
update_count()
{
	char c[16];
	snprintf(c, 16, "%u", cards->len);
	gtk_label_set_text(GTK_LABEL(count), c);
}

static void
read_data_file(const char *file)
{
	char *data;
	char *line;
	char *eol;
	char *args;

	if (!g_file_get_contents(file, &data, NULL, NULL))
		return;

	line = data;

	while ((eol = strchr(line, '\n'))) {
		*eol = '\0';

		if ((args = strchr(line, '\t'))) {
			*args = '\0';
			add_element(line, args + 1);
		}

		line = eol + 1;
	}

	g_free(data);

	update_count();
}

static void
write(card *card, GIOChannel *out)
{
	GError *error = NULL;
	gsize written;
	g_io_channel_write_chars(out, card->hint, -1, &written, &error);
	g_io_channel_write_unichar(out, '\t', &error);
	g_io_channel_write_chars(out, card->truth, -1, &written, &error);
	g_io_channel_write_unichar(out, '\n', &error);
}

static void
write_data_file(const char *file)
{
	GError *error = NULL;
	GIOChannel *out = g_io_channel_new_file(file, "w", &error);
	g_ptr_array_foreach(cards, (GFunc)write, out);
	g_io_channel_shutdown(out, TRUE, &error);
	g_io_channel_unref(out);
}

static void
get_card(gboolean del)
{
	if (del && current) {
		g_strfreev(current->truths);
		g_free(current);
		g_ptr_array_remove_fast(cards, current);
		update_count();
	}

	if (cards->len) {
		current = g_ptr_array_index(cards, random() % cards->len);
		status = WAITING;
		gtk_label_set_text(GTK_LABEL(label), current->hint);
	} else
		gtk_label_set_text(GTK_LABEL(label), "Congratulations, you have finished.");
}

static gboolean
truth_has(const char *needle)
{
	char **toks;
	for (toks = current->truths; *toks; ++toks)
		if (!strcasecmp(*toks, needle))
			return TRUE;
	return FALSE;
}

static void
reveal(const char *color, unsigned s)
{
	char *text = g_strconcat(current->hint, "\n<span foreground='", color,
	                         "'>", current->truth, "</span>", NULL);
	gtk_label_set_markup(GTK_LABEL(label), text);
	free(text);

	status = s;
}

static gboolean
check(GtkWidget *w, gpointer *d)
{
	if (cards->len == 0)
		return FALSE;

	const char *entry = gtk_entry_get_text(GTK_ENTRY(w));

	if (status)
		get_card(status == ADVANCE_AND_DELETE);
	else if (!*entry)
		reveal("red", ADVANCE_AND_KEEP);
	else if (truth_has(entry))
		reveal("green", ADVANCE_AND_DELETE);

	gtk_entry_set_text(GTK_ENTRY(w), "");

	return FALSE;
}

static gboolean
handle_keypress(GtkWidget *w, GdkEventKey *ev, gpointer *d)
{
	switch(ev->keyval) {
	case GDK_Escape:
		gtk_main_quit();
		break;
	case GDK_Tab:
		get_card(TRUE);
		return TRUE;
	case 's':
		if (ev->state & GDK_CONTROL_MASK) {
			write_data_file("state");
			return TRUE;
		}
		break;
	}

	return FALSE;
}

int
main(int argc, char **argv)
{
	GtkWidget *window;
	PangoFontDescription *font;

	gtk_init(&argc, &argv);
	srandom(time(NULL));

	if (argc == 1) {
		fprintf(stderr, "usage: %s data_file1 [data_file2] [data_file3] ...\n", argv[0]);
		return 1;
	}

	cards = g_ptr_array_new();
	current = NULL;
	status = WAITING;

	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	count = gtk_label_new("");
	label = gtk_label_new("Loading...");
	entry = gtk_entry_new();

	font = pango_font_description_new();
	pango_font_description_set_size(font, 24 * PANGO_SCALE);
	gtk_widget_modify_font(label, font);

	gtk_label_set_justify(GTK_LABEL(count), GTK_JUSTIFY_RIGHT);

	gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_CENTER);
	gtk_label_set_selectable(GTK_LABEL(label), TRUE);

	g_signal_connect(GTK_OBJECT(window), "destroy",
	                 GTK_SIGNAL_FUNC(gtk_main_quit), NULL);
	g_signal_connect(G_OBJECT(window), "key-press-event",
	                 G_CALLBACK(handle_keypress), NULL);
	g_signal_connect(G_OBJECT(entry), "activate",
	                 G_CALLBACK(check), NULL);

	GtkWidget *box = gtk_vbox_new(FALSE, 5);

	gtk_box_pack_start(GTK_BOX(box), count, FALSE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(box), label, TRUE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(box), entry, FALSE, TRUE, 0);

	gtk_container_add(GTK_CONTAINER(window), box);
	gtk_widget_show_all(window);

	gtk_widget_grab_focus(entry);

	while (--argc)
		read_data_file(argv[argc]);

	get_card(FALSE);

	gtk_main();
	return 0;
}

